/*=========================================================================

  Program:   TemporalSmoothing
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "itkFixedArray.h"
#include "itkObjectFactory.h"

// Elevation handler
#include "otbWrapperElevationParametersHandler.h"
#include "otbWrapperApplicationFactory.h"

// Application engine
#include "otbStandardFilterWatcher.h"
#include "itkFixedArray.h"

#include <string>
#include "otbIterativeReconstructionSavitzkyGolayInterpolationFunctor.h"
#include "otbSavitzkyGolayVectorImageFunctor.h"

// no data
#include "otbNoDataHelper.h"

// Dates
#include "otbDates.h"

// LayerStack
#include "otbImageListToVectorImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
#include "otbImageList.h"
#include "otbMultiChannelExtractROI.h"

namespace otb
{
namespace Wrapper
{
class TemporalSmoothing : public Application
{
public:
  /** Standard class typedefs. */
  typedef TemporalSmoothing                 Self;
  typedef Application                       Superclass;
  typedef itk::SmartPointer<Self>           Pointer;
  typedef itk::SmartPointer<const Self>     ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);
  itkTypeMacro(TemporalSmoothing, Application);

  /** Dates typedefs */
  typedef itk::VariableLengthVector<int>                                                            DatesType;

  /** Typedefs for image concatenation */
  typedef otb::ImageList<FloatImageType>                                                            ImageListType;
  typedef ImageListToVectorImageFilter<ImageListType, FloatVectorImageType>                         ListConcatenerFilterType;
  typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,
      FloatImageType::PixelType>                                                                    ExtractROIFilterType;
  typedef ObjectList<ExtractROIFilterType>                                                          ExtractROIFilterListType;
  typedef otb::MultiChannelExtractROI<FloatVectorImageType::InternalPixelType,
      FloatVectorImageType::InternalPixelType>                                                      ExtractFilterType;

  /** Savitzky-Golay typedefs */
  typedef otb::Functor::SavitzkyGolayVectorImageFunctor<FloatVectorImageType::PixelType, DatesType> SGFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatVectorImageType, FloatVectorImageType, SGFunctorType>   SGFilterType;

  /** Iterative Reconstruction Savitzky-Golay typedefs */
  typedef otb::Functor::IterativeReconstructionSavistzkyGolayInterpolationFunctor<
      FloatVectorImageType::PixelType, DatesType>                                                   IRSGFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatVectorImageType, FloatVectorImageType, IRSGFunctorType> IRSGFilterType;

private:

  enum FilterMethod
  {
    SAVITZKY_GOLAY,
    ITERATIVE_RECONSTRUCTION_SAVITZKY_GOLAY
  };

  void DoInit() ITK_OVERRIDE
  {
    SetName("TemporalSmoothing");
    SetDescription("Perform temporal smoothing of time series using Savitzky-Golay interpolations");

    // Documentation
    SetDocLimitations("Input images size (rows/cols) must be the same.");
    SetDocAuthors("Rémi Cresson");
    SetDocSeeAlso(" ");

    AddDocTag(Tags::Analysis);
    AddDocTag(Tags::ChangeDetection);
    AddDocTag(Tags::Filter);

    // Mandatory
    AddParameter(ParameterType_InputImageList, "il"     , "Input images (list or stack)");
    AddParameter(ParameterType_InputFilename,  "dates"  , "Dates of images (ASCII file, containing dates in format YYYYMMDD) ");
    AddParameter(ParameterType_OutputImage,    "out"    , "Output Image");

    // No data
    AddParameter(ParameterType_Float,          "nodata" , "Force no data value");
    MandatoryOff("nodata");

    // Interpolation method
    AddParameter(ParameterType_Choice,         "interp" , "Interpolation method");

    // SG
    AddChoice("interp.sg", "Savitzky-Golay method");
    AddParameter(ParameterType_Int,   "interp.sg.deg", "Degree of polynomial fitting");
    MandatoryOff                     ("interp.sg.deg");
    SetDefaultParameterInt           ("interp.sg.deg", 2);
    SetMinimumParameterIntValue      ("interp.sg.deg", 1);
    AddParameter(ParameterType_Int,   "interp.sg.rad", "Radius of polynomial fitting window");
    MandatoryOff                     ("interp.sg.rad");
    SetDefaultParameterInt           ("interp.sg.rad", 2);
    SetMinimumParameterIntValue      ("interp.sg.rad", 1);

    // IRSG
    AddChoice("interp.irsg", "Iterative Reconstruciton Savitzky-Golay method");
    AddParameter(ParameterType_Int,   "interp.irsg.ltdeg", "Degree of polynomial fitting for long-term trend");
    MandatoryOff                     ("interp.irsg.ltdeg");
    SetDefaultParameterInt           ("interp.irsg.ltdeg", 2);
    SetMinimumParameterIntValue      ("interp.irsg.ltdeg", 1);
    AddParameter(ParameterType_Int,   "interp.irsg.ltrad", "Radius of polynomial fitting window for long-term trend");
    MandatoryOff                     ("interp.irsg.ltrad");
    SetDefaultParameterInt           ("interp.irsg.ltrad", 2);
    SetMinimumParameterIntValue      ("interp.irsg.ltrad", 1);
    AddParameter(ParameterType_Int,   "interp.irsg.stdeg", "Degree of polynomial fitting for short-term trend");
    MandatoryOff                     ("interp.irsg.stdeg");
    SetDefaultParameterInt           ("interp.irsg.stdeg", 6);
    SetMinimumParameterIntValue      ("interp.irsg.stdeg", 1);
    AddParameter(ParameterType_Int,   "interp.irsg.strad", "Radius of polynomial fitting window for short-term trend");
    MandatoryOff                     ("interp.irsg.strad");
    SetDefaultParameterInt           ("interp.irsg.strad", 4);
    SetMinimumParameterIntValue      ("interp.irsg.strad", 1);
    AddParameter(ParameterType_Float, "interp.irsg.threshval", "Variations threshold amplitude");
    MandatoryOff                     ("interp.irsg.threshval");
    SetDefaultParameterFloat         ("interp.irsg.threshval", 0.4);
    AddParameter(ParameterType_Float, "interp.irsg.threshtime", "Variations threshold duration (in days)");
    MandatoryOff                     ("interp.irsg.threshtime");
    SetDefaultParameterFloat         ("interp.irsg.threshtime", 20);
    SetMinimumParameterFloatValue    ("interp.irsg.threshtime", 0);

  }

  void DoUpdateParameters()
  {
  }

  /*
   * Retrieve the dates (numeric) from the input dates (string)
   * Input dates (string) must be formated using the following pattern: YYYYMMDD
   */
  DatesType GetTimeSeriesDates(){

    std::vector<otb::dates::SingleDate> calendar = otb::dates::GetDatesFromFile(GetParameterString("dates"));
    int n = calendar.size() - 1;
    otbAppLogINFO("Using " << calendar.size() << " input dates from "
        << calendar[0].year << "/" << calendar[0].month << "/" << calendar[0].day << " to "
        << calendar[n].year << "/" << calendar[n].month << "/" << calendar[n].day );

    n++;

    int first = calendar[0].julianday;
    DatesType dates;
    dates.SetSize(n);
    for (int i = 0 ; i < n ; i++)
      {
      dates[i] = calendar[i].julianday - first + 1;
      }

    return dates;
  }

  void DoExecute()
  {

    // Init. filters
    m_IRSGFilter  = IRSGFilterType::New();
    m_SGFilter    = SGFilterType::New();

    // Get dates
    DatesType dates = GetTimeSeriesDates();

    // Get the NDVI time series input image list
    FloatVectorImageListType::Pointer inNDVIList = this->GetParameterImageList("il");

    // Create one stack for input NDVI images list
    m_Concatener = ListConcatenerFilterType::New();
    m_ImageList = ImageListType::New();
    m_ExtractorList = ExtractROIFilterListType::New();

    // Split each input vector image into image
    // and generate an mono channel image list
    inNDVIList->GetNthElement(0)->UpdateOutputInformation();
    FloatVectorImageType::SizeType size = inNDVIList->GetNthElement(0)->GetLargestPossibleRegion().GetSize();
    for( unsigned int i=0; i<inNDVIList->Size(); i++ )
      {
      FloatVectorImageType::Pointer vectIm = inNDVIList->GetNthElement(i);
      vectIm->UpdateOutputInformation();
      if( size != vectIm->GetLargestPossibleRegion().GetSize() )
        {
        otbAppLogFATAL("Input NDVI image size number " << i << " mismatch");
        }

      for( unsigned int j=0; j<vectIm->GetNumberOfComponentsPerPixel(); j++)
        {
        ExtractROIFilterType::Pointer extractor = ExtractROIFilterType::New();
        extractor->SetInput( vectIm );
        extractor->SetChannel( j+1 );
        extractor->UpdateOutputInformation();
        m_ExtractorList->PushBack( extractor );
        m_ImageList->PushBack( extractor->GetOutput() );
        }
      }
    m_Concatener->SetInput( m_ImageList );
    m_Concatener->UpdateOutputInformation();
    otbAppLogINFO("Size of the time series: " <<
        m_Concatener->GetOutput()->GetNumberOfComponentsPerPixel() );

    // Update input image infos
    unsigned int nbOfChannels = m_Concatener->GetOutput()->GetNumberOfComponentsPerPixel();
    if (nbOfChannels != dates.Size())
      {
      otbAppLogFATAL("Number of dates (" << dates.Size() <<
          ") must be equal to the number of images (" << nbOfChannels << ").");
      }
    otbAppLogINFO("There is " << dates.Size() << " dates.");

    // Handle no-data value
    float noDataValue(.0f);
    if (HasValue("nodata"))
      {
      // Get the user no-data value
      noDataValue = GetParameterFloat("nodata");
      otbAppLogINFO("Using the given no-data value: " << noDataValue);
      }
    else
      {
      // Try to read the no-data in the image metadata
      std::vector<bool>   noDataPresent;
      std::vector<double> noDataValues;
      bool isNoData = otb::ReadNoDataFlags(m_Concatener->GetOutput()->GetImageMetadata(),noDataPresent,noDataValues);
      if (isNoData)
        {
        noDataValue = noDataValues[0];
        otbAppLogINFO("Using no-data value from image metadata: " << noDataValue);
        }
      else
        {
        otbAppLogWARNING("Image metadata does not contain any no-data value!");
        }
      }
    if (vnl_math_isnan(noDataValue))
      {
      otbAppLogFATAL("Please use of a different no-data value than NaN");
      }

    // Setup filters
    if (GetParameterInt("interp") == SAVITZKY_GOLAY)
      {
      otbAppLogINFO("Using simple Savitzky-Golay interpolation");

      m_SGFilter->SetInput(m_Concatener->GetOutput());
      m_SGFilter->GetFunctor().SetDates(dates);
      m_SGFilter->GetFunctor().SetRadius(GetParameterInt("interp.sg.rad"));
      m_SGFilter->GetFunctor().SetDegree(GetParameterInt("interp.sg.deg"));
      m_SGFilter->GetFunctor().SetNoDataValue(noDataValue);
      SetParameterOutputImage("out", m_SGFilter->GetOutput());
      }
    else if (GetParameterInt("interp") == ITERATIVE_RECONSTRUCTION_SAVITZKY_GOLAY)
      {
      otbAppLogINFO("Using iterative reconstruction with Savitzky-Golay interpolation");

      m_IRSGFilter->SetInput(m_Concatener->GetOutput());
      m_IRSGFilter->GetFunctor().SetDates(dates);
      m_IRSGFilter->GetFunctor().SetLongTermRadius(GetParameterInt("interp.irsg.ltrad"));
      m_IRSGFilter->GetFunctor().SetLongTermDegree(GetParameterInt("interp.irsg.ltdeg"));
      m_IRSGFilter->GetFunctor().SetShortTermRadius(GetParameterInt("interp.irsg.strad"));
      m_IRSGFilter->GetFunctor().SetShortTermDegree(GetParameterInt("interp.irsg.stdeg"));
      m_IRSGFilter->GetFunctor().SetThresholdAmplitude(GetParameterFloat("interp.irsg.threshval"));
      m_IRSGFilter->GetFunctor().SetThresholdDuration(GetParameterFloat("interp.irsg.threshtime"));
      m_IRSGFilter->GetFunctor().SetNoDataValue(noDataValue);
      SetParameterOutputImage("out", m_IRSGFilter->GetOutput());
      }
    else
      {
      otbAppLogFATAL("Unknown interp parameter");
      }
  }

  // Stack
  ListConcatenerFilterType::Pointer   m_Concatener;
  ExtractROIFilterListType::Pointer   m_ExtractorList;
  ImageListType::Pointer              m_ImageList;
  ExtractFilterType::Pointer          m_ExtractChannelsFilter;

  // Temporal filtering
  IRSGFilterType::Pointer             m_IRSGFilter;
  SGFilterType::Pointer               m_SGFilter;
};


} // namespace wrapper
} // namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::TemporalSmoothing)

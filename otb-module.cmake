set(DOCUMENTATION "Temporal smoothing")

otb_module(TemporalSmoothing
  DEPENDS
    OTBITK
    OTBCommon
    OTBApplicationEngine
    OTBTimeSeries
    TimeSeriesUtils
    	
  TEST_DEPENDS
    OTBTestKernel
    OTBCommandLine
  DESCRIPTION
    "Contains one application dedicated to time series smoothing"
)

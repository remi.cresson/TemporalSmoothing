/*=========================================================================

  Program:   TemporalSmoothing
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  Some portions of the code from otbSavitzkygolayInterpolationFunctor.h (OTB)

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBSAVITZKYGOLAYVECTORIMAGEFUNCTOR_H_
#define MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBSAVITZKYGOLAYVECTORIMAGEFUNCTOR_H_

#include "otbPixelValuesLinearInterpolation.h"

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>

namespace otb
{
namespace Functor
{

/** \class PolynomialTimeSeriesFunction
 *
 *  \brief: Compute a polynomial model, and return its value at the 'offset' position.
 *  Rely on BOOST library.
 *  Core function adapted from Vili Petek blog [1]
 *
 *  [1] http://vilipetek.com/2013/10/07/polynomial-fitting-in-c-using-boost/
 *
 * \ingroup TemporalSmoothing
 */

template <class TSeriesType, class TDatesType, class TPrecisionType>
class PolynomialTimeSeriesFunction
{

public:
  // Constructor
  PolynomialTimeSeriesFunction(){};

  // Destructor
  ~PolynomialTimeSeriesFunction(){};

  // Set dates
  void SetDates(TDatesType& dates)
  {
    m_Dates = dates;
  }

  // Set degree
  void SetDegree(unsigned int degree)
  {
    m_Degree = degree;
  }

  // Set offset (The index of the serie to be computed)
  void SetOffset(unsigned int offset)
  {
    m_Offset = offset;
  }

  // Operator
  inline TPrecisionType operator ()(TSeriesType& series)
  {

    using namespace boost::numeric::ublas;

    // more intuitive this way
    const unsigned int nDegree = m_Degree + 1;

    size_t nCount =  series.Size();
    matrix<TPrecisionType> oXMatrix( nCount, nDegree );
    matrix<TPrecisionType> oYMatrix( nCount, 1 );

    // copy y matrix
    for ( size_t i = 0; i < nCount; i++ )
      {
      oYMatrix(i, 0) = series[i];
      }

    // create the X matrix
    for ( size_t nRow = 0; nRow < nCount; nRow++ )
      {
      TPrecisionType nVal = 1.0f;
      for ( unsigned int nCol = 0; nCol < nDegree; nCol++ )
        {
        oXMatrix(nRow, nCol) = nVal;
        nVal *= static_cast<TPrecisionType>(m_Dates[nRow]);
        }
      }

    // transpose X matrix
    matrix<TPrecisionType> oXtMatrix( trans(oXMatrix) );
    // multiply transposed X matrix with X matrix
    matrix<TPrecisionType> oXtXMatrix( prec_prod(oXtMatrix, oXMatrix) );
    // multiply transposed X matrix with Y matrix
    matrix<TPrecisionType> oXtYMatrix( prec_prod(oXtMatrix, oYMatrix) );

    // lu decomposition
    permutation_matrix<int> pert(oXtXMatrix.size1());
    const std::size_t singular = lu_factorize(oXtXMatrix, pert);

    // must be singular
    BOOST_ASSERT( singular == 0 );

    // backsubstitution
    lu_substitute(oXtXMatrix, pert, oXtYMatrix);

    // copy the result to coeff
    std::vector<TPrecisionType> oCoeff( oXtYMatrix.data().begin(), oXtYMatrix.data().end() );

    // Compute value at m_Offset
    TPrecisionType nY = 0;
    TPrecisionType nXT = 1;
    TPrecisionType nX = static_cast<TPrecisionType>(m_Dates[m_Offset]);
    for ( size_t j = 0; j < nDegree; j++ )
      {
      // multiply current x by a coefficient
      nY += oCoeff[j] * nXT;
      // power up the X
      nXT *= nX;
      }

    return nY;
  }

private:
  unsigned int m_Degree;
  unsigned int m_Offset;
  TDatesType m_Dates;

};

/** \class otbSavitzkyGolayVectorImageFunctor
 *
 *  \brief: This functor implements a local polynomial regression (of
 *  degree k) on a series of values (of at least k+1 points which are
 *  treated as being equally spaced in the series) to determine the
 *  smoothed value for each point.
 *
 *  In this implementation, the interpolation is performed by least
 *  squares fitting. The size of the moving window for the
 *  interpolation can be set using the radius variable.
 *
 *  Savitzky, A.; Golay, M.J.E. (1964). "Smoothing and Differentiation of
 *  Data by Simplified Least Squares Procedures". Analytical Chemistry 36
 *  (8): 1627-1639. doi:10.1021/ac60214a047
 *
 * \ingroup TemporalSmoothing
 */
template <class TSeries, class TDates>
class SavitzkyGolayVectorImageFunctor
{
public:

  typedef typename TSeries::ValueType ValueType;
  typedef typename TDates::ValueType DateType;

  typedef double CoefficientPrecisionType;
  typedef PixelValuesLinearInterpolation<TSeries> PVLIFunctorTyle;
  typedef PolynomialTimeSeriesFunction<TSeries, TDates, CoefficientPrecisionType > TSFunctionType;

  /// Constructor
  SavitzkyGolayVectorImageFunctor()
  {
    m_Radius = 2;
    m_Degree = 2;
    m_NoDataValue = 0;
    m_FillNoDataValues = true;
  }

  /// Destructor
  virtual ~SavitzkyGolayVectorImageFunctor() {}

  // Set radius
  void SetRadius(unsigned int radius)
  {
    m_Radius = radius;
  }

  // Set dates
  void SetDates(TDates& dates)
  {
    m_Dates = dates;
  }

  // Set degree
  void SetDegree(unsigned int degree)
  {
    m_Degree = degree;
  }

  // Set no data value
  void SetNoDataValue(ValueType value)
  {
    m_NoDataValue = value;
  }

  // Set fill hode mode
  void SetFillNoDataValues(bool value)
  {
    m_FillNoDataValues = value;
  }

  inline TSeries operator ()(const TSeries& series)
  {
    unsigned int nbDates = series.Size();
    unsigned int firstSample = m_Radius;
    unsigned int lastSample = nbDates - m_Radius - 1; // must be >=0, i.e. nbDates > m_Radius

    // Copy the entire input signal
    TSeries gapfilledSeries;
    if (m_FillNoDataValues)
      {
      // Replace no-data values using linear interpolation
      PVLIFunctorTyle fillHoleFunctor;
      fillHoleFunctor.SetIgnoreValue(m_NoDataValue);
      gapfilledSeries = fillHoleFunctor(series);

//      // Update first and last samples indices
//      unsigned int firstFound = nbDates;
//      unsigned int lastFound = 0;
//      for (unsigned int i = 0 ; i < nbDates ; i++)
//        {
//        if (series[i] != m_NoDataValue && i < firstFound)
//          firstFound = i;
//        unsigned int ri = nbDates-i;
//        if (series[ri] != m_NoDataValue && ri > lastFound)
//          lastFound = ri;
//        }
//      firstSample = vcl_max(firstSample, firstFound);
//      lastSample = vcl_min(lastSample, lastFound);
      }
    else
      {
      gapfilledSeries = TSeries(series);
      }

    // Replace the middle part with the smoothed values
    TSeries outSeries(gapfilledSeries);
    for(unsigned int i = firstSample; i <= lastSample; ++i)
      {
      TSeries tmpInSeries;
      tmpInSeries.SetSize(2*m_Radius+1);
      TDates tmpDates;
      tmpDates.SetSize(2*m_Radius+1);

      for(unsigned int j = 0; j <= 2*m_Radius; ++j)
        {
        tmpInSeries[j] = gapfilledSeries[i+j-m_Radius];
        tmpDates[j] = m_Dates[i+j-m_Radius] - m_Dates[i+m_Radius]; // centering dates to avoid numerical issues caused by julian day values (10e8)
        }

      // Polynomial fitting over (X,Y)=(tmpdates, tmpInSeries)
      TSFunctionType f;
      f.SetDegree(m_Degree);
      f.SetOffset(m_Radius);
      f.SetDates(tmpDates);
      outSeries[i] = f(tmpInSeries);
      }

    return outSeries;
  }

private:

  unsigned int  m_Radius;
  unsigned int  m_Degree;
  TDates        m_Dates;
  bool          m_FillNoDataValues;
  ValueType     m_NoDataValue;

};

} // ns functor
} // ns otb

#endif /* MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBSAVITZKYGOLAYVECTORIMAGEFUNCTOR_H_ */

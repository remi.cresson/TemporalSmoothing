/*=========================================================================

  Program:   TemporalSmoothing
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBITERATIVERECONSTRUCTIONSAVITZKYGOLAYINTERPOLATIONFUNCTOR_H_
#define MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBITERATIVERECONSTRUCTIONSAVITZKYGOLAYINTERPOLATIONFUNCTOR_H_


#include "otbSavitzkyGolayVectorImageFunctor.h"
#include "otbPixelValuesLinearInterpolation.h"

namespace otb
{
namespace Functor
{
/** \class IterativeReconstructionSavistzkyGolayInterpolationFunctor
 *
 *  \brief: This class implements a Savitzky-Golay recursive interpolation
 *  fitting the upper envelope of the series being interpolated, described in
 *  Chen, J., Jönsson, P., Tamura, M., Gu, Z., Matsushita, B., & Eklundh, L. (2004).
 *  A simple method for reconstructing a high-quality NDVI time-series data set
 *  based on the Savitzky–Golay filter. Remote sensing of Environment, 91(3), 332-344.
 *
 * \ingroup TemporalSmoothing
 */
template <class TSeries, class TDates>
class IterativeReconstructionSavistzkyGolayInterpolationFunctor
{
public:

  typedef SavitzkyGolayVectorImageFunctor<TSeries, TDates> SGFunctorType;
  typedef PixelValuesLinearInterpolation<TSeries> PVLIFunctorTyle;
  typedef typename TSeries::ValueType SerieValueType;
  typedef typename TDates::ValueType DateValueType;

  /// Constructor
  IterativeReconstructionSavistzkyGolayInterpolationFunctor()
  {
    m_MaximumNumberOfIterations = 100;
    m_LongTermDegree= 6;
    m_LongTermRadius = 4;
    m_ShortTermDegree = 2;
    m_ShortTermRadius = 2;
    m_ThresholdAmplitude = 0.4;
    m_NoDataValue = 0;
    m_ThresholdDuration = 20;
  }

  /// Destructor
  virtual ~IterativeReconstructionSavistzkyGolayInterpolationFunctor() {}

  inline void SetLongTermDegree(const unsigned int degree)
  {
    m_LongTermDegree = degree;
  }

  inline void SetShortTermDegree(const unsigned int degree)
  {
    m_ShortTermDegree = degree;
  }

  inline void SetLongTermRadius(const unsigned int Radius)
  {
    m_LongTermRadius = Radius;
  }

  inline void SetShortTermRadius(const unsigned int Radius)
  {
    m_ShortTermRadius = Radius;
  }

  inline void SetDates(const TDates datesInDays)
  {
    m_Dates = datesInDays ;
  }

  inline void SetMaximumNumberOfIterations(unsigned int its)
  {
    m_MaximumNumberOfIterations = its;
  }

  inline void SetThresholdAmplitude(SerieValueType value)
  {
    m_ThresholdAmplitude = value;
  }

  inline void SetThresholdDuration(SerieValueType value)
  {
    // envelope
    m_ThresholdDuration = value;
  }

  inline void SetNoDataValue (SerieValueType value)
  {
    m_NoDataValue = value;
  }

  inline TSeries operator ()(const TSeries& series)
  {

    unsigned int nDates = series.Size();

    // Set no data value where there is an increase of at least
    // m_Treshold during a time window shorted than m_ThresholdDuration
    TSeries maskedSeries(series);
    for (unsigned int i = 0 ; i < nDates-1 ; i++)
      {
      if (maskedSeries[i] != m_NoDataValue)
        {
        // Look in the future and check there is no odd increase
        for (unsigned int j = i + 1; j < nDates; j++)
          {
          if (m_Dates[j] - m_Dates[i] <= m_ThresholdDuration)
            {
            if ( (maskedSeries[j] != m_NoDataValue) && (maskedSeries[j] - maskedSeries[i] > m_ThresholdAmplitude))
              {
              // We mark this sample as invalid and process the next one
              maskedSeries[i] = m_NoDataValue;
              break;
              }
            }
          else
            {
            // Exit the loop since we are out of time window
            break;
            }
          }
        } // current sample is different than no data
      }

    // Fill no data values using linear interpolation
    PVLIFunctorTyle fillHolesInterp;
    fillHolesInterp.SetIgnoreValue(m_NoDataValue);
    TSeries interp_ts = fillHolesInterp(maskedSeries);

    // Set up functor
    SGFunctorType sgFunctor;
    sgFunctor.SetDates(m_Dates);
    sgFunctor.SetDegree(m_LongTermDegree);
    sgFunctor.SetRadius(m_LongTermRadius);
    sgFunctor.SetFillNoDataValues(false);

    // Iteration
    float f = 1.0e8;
    unsigned int iter = 0;
    TSeries w, smoother_ts, smooth_ts(interp_ts);
    w.SetSize(nDates);
    w.Fill(1);
    while (iter < m_MaximumNumberOfIterations)
      {

      // Smooth the series
      smoother_ts = sgFunctor(smooth_ts);

      // Compute the difference d and max(d)
      TSeries d(smoother_ts);
      SerieValueType dmax = 0;
      for (unsigned int i = 0 ; i < nDates ; i++)
        {
        d[i] -= interp_ts[i];
        SerieValueType abs_d = vcl_abs( d[i] );
        if (abs_d > dmax)
          dmax = abs_d;
        }

      if (iter == 0)
        {
        // Set the short term parameters for S-G interpolation
        sgFunctor.SetDegree(m_ShortTermDegree);
        sgFunctor.SetRadius(m_ShortTermRadius);

        // Compute w
        for (unsigned int i = 0 ; i < nDates ; i++)
          {
          if (d[i] > 0)
            {
            w[i] = 1 - d[i] / dmax;
            }
          }
        }

      // Compute fitting-effect index
      float fk = .0f;
      for (unsigned int i = 0 ; i < nDates ; i++)
        {
        fk += vcl_abs( d[i] ) * w[i];
        }

      // Condition for exiting
      if (fk > f)
        break;

      // Replace values in smoother_ts
      for (unsigned int i = 0 ; i < nDates ; i++)
        {
        if (d[i] < 0)
          {
          smooth_ts[i] = interp_ts[i];
          }
        else
          {
          smooth_ts[i] = smoother_ts[i];
          }
        }

      f = fk;
      iter++;
      } // while

    return smooth_ts;
  }

private:

  TDates            m_Dates;
  unsigned int      m_LongTermDegree;
  unsigned int      m_ShortTermDegree;
  unsigned int      m_LongTermRadius;
  unsigned int      m_ShortTermRadius;
  unsigned int      m_MaximumNumberOfIterations;
  SerieValueType    m_ThresholdAmplitude;
  SerieValueType    m_NoDataValue;
  DateValueType     m_ThresholdDuration;
};
}
} //namespace otb


#endif /* MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBITERATIVERECONSTRUCTIONSAVITZKYGOLAYINTERPOLATIONFUNCTOR_H_ */

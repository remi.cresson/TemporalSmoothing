/*=========================================================================

  Program:   TemporalSmoothing
  Language:  C++

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.

  See LICENSE for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#ifndef MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBPIXELVALUESLINEARINTERPOLATION_H_
#define MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBPIXELVALUESLINEARINTERPOLATION_H_

#include <vector>

namespace otb
{
namespace Functor
{

/** \class PixelValuesLinearInterpolation
 *
 *  \brief: This functor replaces holes in the pixel values with
 *  the linear interpolation of the nearest left and right ones.
 *
 * \ingroup TemporalSmoothing
 */
template <class TSeries>
class PixelValuesLinearInterpolation
{
public:

  typedef typename TSeries::ValueType SerieValueType;

  // Constructor
  PixelValuesLinearInterpolation(): m_NoDataValue(0) {};

  // Destructor
  ~PixelValuesLinearInterpolation(){};

  inline void SetIgnoreValue (SerieValueType value)
  {
    m_NoDataValue = value;
  }

  inline TSeries operator ()(const TSeries& series)
  {

    TSeries interpSeries(series);
    int nDates = series.Size();

    if (nDates > 1)
      {
      // Mark the series indices that are around a hole
      std::vector<int> valableIndices;

      bool insideHole = (series[0] == m_NoDataValue);
      if (insideHole)
        valableIndices.push_back(-1);
      for (int i = 1 ; i < nDates ; i++)
        {
        if (series[i] != m_NoDataValue)
          {
          if (insideHole)
            {
            valableIndices.push_back(i);
            insideHole = false;
            }
          }
        else
          {
          if (!insideHole)
            {
            valableIndices.push_back(i-1);
            insideHole = true;

            }
          }
        }

      // We enter this if there is at least 1 hole
      unsigned int nbOfHoles = valableIndices.size()/2;
      if (nbOfHoles > 0)
        {
        // Fill the gaps between valableIndices
        for (unsigned int hole = 0; hole < nbOfHoles ; hole++)
          {
          // Indices of hole range
          int ltIndice = valableIndices[2*hole];
          int rtIndice = valableIndices[2*hole+1];

          if (ltIndice > -1)
            {
            // Linear interpolation
            for (int k = ltIndice + 1; k < rtIndice ; k++)
              {
              SerieValueType a = (series[rtIndice] - series[ltIndice]) / (rtIndice - ltIndice);
              SerieValueType b = series[ltIndice] - a * ltIndice;
              interpSeries[k] = a * k + b;
              } // interpolation
            } // avoid borders
          } // iteration over holes
        }// number of holes > 0
      } // nDates > 1

    return interpSeries;
  }
private:
  SerieValueType m_NoDataValue;

}; // class PixelValuesLinearInterpolation

} // namespace functor
} // namespace otb

#endif /* MODULES_REMOTE_MULTITEMPORALSMOOTHING_INCLUDE_OTBPIXELVALUESLINEARINTERPOLATION_H_ */

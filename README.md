# Temporal Smoothing

This module contains an application dedicated to time series smoothing.

##  What it is

This application filters time series. There is two available algorithms:
* The Savitzky-Golay interpolation
* The [Chen et al method](https://doi.org/10.1016/j.rse.2004.03.014) method, based on a Savitzky-Golay iterative reconstruction approach.

## Dependencies

* The [OTB](https://www.orfeo-toolbox.org/) library
* The [TimeSeriesUtils](https://gitlab.irstea.fr/remi.cresson/TimeSeriesUtils) remote module for OTB

## How to build it

The module can be built like any other [otb remote module](https://wiki.orfeo-toolbox.org/index.php/How_to_write_a_remote_module). You can build it either from within OTB's sources or outside it.

## How to use it

TemporalSmoothing provides an OTBApplication.

```
This is the TemporalSmoothing application, version 6.0.0
Perform temporal smoothing of time series

Parameters: 
        -progress               <boolean>        Report progress 
MISSING -in                     <string>         Input Image  (mandatory)
MISSING -dates                  <string list>    Dates of images (Must be dd/mm/yyyy format)  (mandatory)
MISSING -out                    <string> [pixel] Output Image  [pixel=uint8/uint16/int16/uint32/int32/float/double] (default value is float) (mandatory)
        -nodata                 <float>          Force no data value  (optional, off by default)
        -interp                 <string>         Interpolation method [sg/irsg] (mandatory, default value is sg)
        -interp.sg.deg          <int32>          Degree of polynomial fitting  (optional, on by default, default value is 2)
        -interp.sg.rad          <int32>          Radius of polynomial fitting window  (optional, on by default, default value is 2)
        -interp.irsg.ltdeg      <int32>          Degree of polynomial fitting for long-term trend  (optional, on by default, default value is 2)
        -interp.irsg.ltrad      <int32>          Radius of polynomial fitting window for long-term trend  (optional, on by default, default value is 2)
        -interp.irsg.stdeg      <int32>          Degree of polynomial fitting for short-term trend  (optional, on by default, default value is 6)
        -interp.irsg.strad      <int32>          Radius of polynomial fitting window for short-term trend  (optional, on by default, default value is 4)
        -interp.irsg.threshval  <float>          Variations threshold amplitude  (optional, on by default, default value is 0)
        -interp.irsg.threshtime <float>          Variations threshold duration (in days)  (optional, on by default, default value is 20)
        -inxml                  <string>         Load otb application from xml file  (optional, off by default)
```

* Input image can be either a list of images, or stacks of images.
* Input dates must be in DD/MM/YYYY format.
* The "_interp_" parameter enables the selection of the temporal interpolation method: Savitzky-Golay ("_sg_") or iterative reconstruction method based on Savitzky-Golay interpolation ("_irsg_").
* If the signal has a bump of more than "_interp.irsg.threshval_" in a time lesser than "_interp.irsg.threshtime_", then the lower pixel is considered as corrupted (eg clouds).
* The "_nodata_" parameter enables the change of the no-data value used at input.

## License

Please see the license for legal issues on the use of the software (CeCILL).

## Contact

Rémi Cresson (IRSTEA)

